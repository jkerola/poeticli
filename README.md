# PoetiCLI

Write poetry on your CLI!

## Installation

Download the latest release [from the gitlab repository.](https://gitlab.com/jkerola/poeticli/)

Unzip the poeticli folder to a location of your choice and add the location to your path:

```shell
export PATH=$PATH:/path/to/poeticli/
```

then verify the installation by running

```shell
poeticli about
```

## Documentation

Work in progress, please check [the gitlab repository for up to date information.](https://gitlab.com/jkerola/poeticli)

All currently available commands can be displayed with

```shell
poeticli --help
```

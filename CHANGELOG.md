# :star2: Changelog 


### [v0.1.1](https://gitlab.com/jkerola/poeticli/compare/v0.1.0...v0.1.1) (2022-02-16)


#### Features

* add --version flag
 fe6d89a


## v0.1.0 (2022-02-03)


### Features

* add basic functionality
 7a78447

* initial commit
 0b11c4a


// Dart imports:
import 'dart:io';

// Package imports:
import 'package:args/command_runner.dart';
import 'package:cli_util/cli_logging.dart';
import 'package:hive/hive.dart';
import 'package:path/path.dart' as path;

// Project imports:
import 'commands/about_command.dart';
import 'commands/clean_command.dart';
import 'commands/config_command.dart';
import 'commands/poem_command.dart';
import 'commands/show_command.dart';
import 'models/arg_strings.dart';
import 'models/poem_obj.dart';
import 'utils/generic_utils.dart' show createLogger;

// consts
final ArgStrings argStrings = ArgStrings();
final rootDir = argStrings.rootDir;

void main(List<String> arguments) async {
  final Logger logger = createLogger(arguments);

  // TODO exception for no permissions to create files
  Hive.init(path.join(rootDir, '.hive'));

  // Register adapters
  Hive.registerAdapter(PoemAdapter());
  var configBox = await Hive.openBox('config');

  // Simple check for pre-existing configuration
  // If no key 'configuration' exists, then run first time setup
  configBox.get('configuration') ?? await setupConfig();

  // Register commands
  final runner = CommandRunner(argStrings.appName, argStrings.appDescription)
    ..addCommand(PoemCommand())
    ..addCommand(ShowCommand())
    ..addCommand(ConfigCommand())
    ..addCommand(AboutCommand())
    ..addCommand(CleanCommand())
    ..argParser.addFlag(argStrings.argVersion,
        abbr: 'v', help: argStrings.helpVersion, negatable: false);

  // Parse for version flag
  try {
    final results = runner.parse(arguments);
    if (results.wasParsed(argStrings.argVersion)) {
      logger.stdout('v${argStrings.currentVersion}');
      exit(1);
    }
  } on Exception catch (error) {
    logger.stderr(error.toString());
    exit(0);
  }

  // Finally parse other arguments
  runner.run(arguments).catchError((error) {
    if (error is FileSystemException) {
      logger.stdout(argStrings.errFileSystemPermissions);
    } else {
      logger.stderr(error.toString());
    }
  });
}

// TODO Move this function to appropriate util file
Future setupConfig() async {
  Logger logger = createLogger([]);
  Progress progress = logger.progress('Creating default configuration');

  // Create default configuration
  var configBox = await Hive.openBox('config');
  configBox.put('configuration', 'default');
  configBox.put('editor', 'nano');
  return progress.finish(
    message: 'OK',
    showTiming: true,
  );
}

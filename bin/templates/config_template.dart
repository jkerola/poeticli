String configTemplate = """
Configuration path {{path}}

Current configuration [{{config['configuration']}}]:
{% for key, value in config %}{% if key != 'init' %} {{ key }} = {{ value }}{% endif %}
{% endfor %}
Use 'poeticli config --help' to display available commands.
"""
    .trim();

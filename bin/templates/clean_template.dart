String cleanTemplate = '''
Current collection holds {{count}} poems and dates as far back as {{date}}.

Run "poeticli clean --help" to see available options.
'''
    .trim();

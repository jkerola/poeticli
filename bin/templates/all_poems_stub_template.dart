String allPoemsStubTemplate = '''
Poems:
 ID\t| Date\t\t\t| Poem
------------------------------------------
{% for poem in poems %} {{poem.id}}\t| {{poem.date}}\t| {{poem.content}}\n{% endfor %}
Current collection holds {{count}} poem{%if count > 1%}s{% endif %}.
'''
    .trim();

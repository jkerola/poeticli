String aboutTemplate = """
{{name}} v{{version}}
{{description}}

This tool is intended for anyone wishing to collect the momentary poetical thoughts drifting through their daily lives.

The application is licensed with GNU General Public License v3.0 (GPLv3). Please see the repository for more details: {{repository}}
If you have any suggestions or feedback, please don't hesitate to contact the author.
"""
    .trim();

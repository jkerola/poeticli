// Dart imports:
import 'dart:io';

// Package imports:
import 'package:cli_util/cli_logging.dart';
import 'package:pubspec_parse/pubspec_parse.dart';

// Project imports:
import '../models/arg_strings.dart';

// consts
final ArgStrings argStrings = ArgStrings();

Pubspec parsePubspec() {
  return Pubspec.parse(File('./pubspec.yaml').readAsStringSync());
}

// Returns an appropriate logger depending on wether the --verbose argument was passed
Logger createLogger(List<String> args) {
  return args.contains(argStrings.argVerbose)
      ? Logger.verbose()
      : Logger.standard();
}

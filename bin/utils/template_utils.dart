// Package imports:
import 'package:cli_util/cli_logging.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:jinja/jinja.dart';
import 'package:pubspec_parse/pubspec_parse.dart';

// Project imports:
import '../templates/about_template.dart';
import '../templates/all_poems_detail_template.dart';
import '../templates/all_poems_stub_template.dart';
import '../templates/clean_template.dart';
import '../templates/config_template.dart';
import '../templates/poem_template.dart';
import 'generic_utils.dart';

// consts
final env = Environment();
final Logger logger = createLogger([]);
final dateFormatString = 'MMMM dd, yyyy ~';
final shortDateFormatString = 'MMMM dd, yyyy';

void displayAbout() {
  final Pubspec pubspec = parsePubspec();
  var template = env.fromString(aboutTemplate).render(
      name: 'PoetiCLI',
      description: pubspec.description,
      version: pubspec.version,
      repository: pubspec.repository);
  logger.stdout(template);
}

void displayConfig() async {
  var configBox = await Hive.openBox('config');
  var path = configBox.path;
  var config = configBox.toMap();
  var template =
      await env.fromString(configTemplate).render(config: config, path: path);
  logger.stdout(template);
}

void displayClean() async {
  var poemBox = await Hive.openBox('poems');
  DateFormat formatter = DateFormat(shortDateFormatString);
  var template = await env.fromString(cleanTemplate).render(
      count: poemBox.length, date: formatter.format(poemBox.getAt(0).date));
  logger.stdout(template);
}

void displayShowAll() async {
  var poemBox = await Hive.openBox('poems');
  DateFormat formatter = DateFormat(shortDateFormatString);
  var poemVals = poemBox.toMap().values.toList();
  var poems = [];
  for (var poem in poemVals) {
    int cutOffLength = 25;
    int length = poem.content.toString().length < cutOffLength
        ? poem.content.toString().length
        : cutOffLength;
    var stub = poem.content.toString().substring(0, length).trim();
    stub += poem.content.length >= cutOffLength ? '...' : '';
    poems.add({
      'content': stub,
      'date': formatter.format(poem.date),
      'id': poemVals.indexOf(poem),
    });
  }
  var template = await env
      .fromString(allPoemsStubTemplate)
      .render(poems: poems, count: poems.length);
  logger.stdout(template);
}

void displayPoem(poem) async {
  DateFormat formatter = DateFormat(dateFormatString).add_jm();
  var formattedPoem = {
    'content': poem.content,
    'date': formatter.format(poem.date),
  };
  var template = await env.fromString(poemTemplate).render(poem: formattedPoem);
  logger.stdout(template);
}

void displayShowAllDetail() async {
  var poemBox = await Hive.openBox('poems');
  DateFormat formatter = DateFormat(dateFormatString).add_jm();
  var poemVals = poemBox.toMap().values.toList();
  var poems = [];
  for (var poem in poemVals) {
    poems.add({
      'content': poem.content,
      'date': formatter.format(poem.date),
      'id': poemVals.indexOf(poem),
    });
  }
  var template =
      await env.fromString(allPoemsDetailTemplate).render(poems: poems);
  logger.stdout(template);
}

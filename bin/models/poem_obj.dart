// Package imports:
import 'package:hive/hive.dart';

part 'poem_obj.g.dart';

@HiveType(typeId: 0)
class Poem extends HiveObject {
  @HiveField(0)
  late String content;

  @HiveField(1)
  late DateTime date;
}

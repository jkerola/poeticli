// Package imports:
import 'package:dcli/dcli.dart' show DartScript;

// Project imports:
import '../utils/generic_utils.dart';

class ArgStrings {
  // Config
  final rootDir = DartScript.self.pathToScriptDirectory;
  final appName = 'poeticli';
  final appDescription = 'Write poetry on your CLI!';

  // Version flag
  final currentVersion = parsePubspec().version;

  // Argument strings
  final argVersion = 'version';
  final argHelp = 'help';
  final argVerbose = 'verbose';
  final argPoem = 'poem';
  final argAbout = 'about';
  final argConfig = 'config';
  final argClean = 'clean';
  final argShow = 'show';

  // Help Strings
  final helpPoem = "Compose a new poem with the default editor.";
  final helpAbout = 'Display more information about the application.';
  final helpConfig = 'Configure PoetiCLI settings.';
  final helpClean = 'Delete a poem or collection from storage.';
  final helpShow =
      'Display all poems or a speficic poem from current collection.';

  // Flags help Strings
  final helpHelp = "Display more information.";
  final helpVerbose = "Display statistics during execution.";
  final helpVersion = "Display build version information.";

  // Config Command args
  final poemEditorArg = 'editor';
  final poemEditorHelp =
      "Change the default text editing tool. Examples: 'nano', 'vim'.";
  final configCleanArg = 'clean';
  final configCleanHelp = 'Delete the current configuration file from disk.';

  // Show command args
  final showAllArg = 'all';
  final showAllHelp = 'Display all poems in current collection.';
  final showPoemArg = 'poem';
  final showPoemHelp = 'Display specified poem in full.';

  // Clean command args
  final cleanCollectionArg = 'collection';
  final cleanCollectionHelp = 'Delete the entire poem collection from disk.';
  final cleanCollectionConfirm =
      'Warning: This will remove all poems from storage. This operation cannot be reversed. Proceed? [y/N]: ';
  final cleanPoemArg = 'poem';
  final cleanPoemHelp = 'Delete a specified poem from the current collection.';

  // Errors
  final errEditor =
      'PoetiCLI did not exit properly. Your thoughts may have been lost.';
  final errFileSystemPermissions =
      'PoetiCLI requires permissions to write in the install directory.\nRun the following commands to fix this:\n sudo chown ${DartScript.self.pathToScriptDirectory} \$USER';

  // Misc
  final infoEditor =
      '\n# Write your thoughts here. Lines starting with "#" will be ignored,\n# and closing an empty file will cancel the process.\n# Press ctrl + s to save your changes, then press ctrl + x to exit.';
}

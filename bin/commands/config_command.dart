// Package imports:
import 'package:args/command_runner.dart';
import 'package:hive/hive.dart';

// Project imports:
import '../models/arg_strings.dart';
import '../utils/template_utils.dart';

// consts
final argStrings = ArgStrings();

class ConfigCommand extends Command {
  @override
  final name = argStrings.argConfig;
  @override
  final description = argStrings.helpConfig;

  ConfigCommand() {
    argParser.addOption(argStrings.poemEditorArg,
        help: argStrings.poemEditorHelp);
    argParser.addFlag(argStrings.configCleanArg,
        help: argStrings.configCleanHelp, negatable: false);
  }
  @override
  void run() async {
    var configModified = false;
    var configBox = await Hive.openBox('config');
    // Check wether options or flags were passed.
    if (argResults!.arguments.isNotEmpty) {
      configModified = true;
      if (argResults!.wasParsed(argStrings.poemEditorArg)) {
        // TODO check wether editor is available
        configBox.put('editor', argResults?[argStrings.poemEditorArg]);
        logger.stdout('Set editor to ${argResults?[argStrings.poemEditorArg]}');
      }
      if (argResults!.wasParsed(argStrings.configCleanArg)) {
        var progress = logger.progress("Removing configuration file");
        await configBox.deleteFromDisk();
        progress.finish(message: 'Done.', showTiming: true);
        configModified = false;
      }
    } else {
      // If no options passed, display help
      displayConfig();
    }
    if (configModified) {
      configBox.put('configuration', 'custom');
    }
  }
}

// Dart imports:
import 'dart:io';

// Package imports:
import 'package:args/command_runner.dart';
import 'package:cli_util/cli_logging.dart';
import 'package:hive/hive.dart';

// Project imports:
import '../models/arg_strings.dart';
import '../utils/template_utils.dart';

// consts
final argStrings = ArgStrings();

class CleanCommand extends Command {
  @override
  final name = argStrings.argClean;
  @override
  final description = argStrings.helpClean;

  CleanCommand() {
    argParser.addFlag(argStrings.cleanCollectionArg,
        help: argStrings.cleanCollectionHelp, negatable: false);
    argParser.addOption(argStrings.cleanPoemArg,
        help: argStrings.cleanPoemHelp, abbr: 'p');
  }
  @override
  void run() async {
    if (argResults!.wasParsed(argStrings.cleanCollectionArg)) {
      logger.write(argStrings.cleanCollectionConfirm);
      String? confirm = stdin.readLineSync();
      if (confirm!.toLowerCase() == 'y') {
        Progress progress = logger.progress('Deleting collection');
        var poemBox = await Hive.openBox('poems');
        await poemBox.deleteFromDisk();
        progress.finish(message: 'Done.');
      } else {
        logger.stdout('Operation cancelled.');
      }
    } else if (argResults!.wasParsed(argStrings.cleanPoemArg)) {
      try {
        var poemId = int.parse(argResults![argStrings.cleanPoemArg]);
        var poemBox = await Hive.openBox('poems');

        await poemBox.getAt(poemId);
        Progress progress = logger.progress('Deleting poem');
        poemBox.deleteAt(poemId);
        progress.finish(message: 'Done.');
      } catch (error) {
        logger.stderr(error.toString());
      }
    } else {
      displayClean();
    }
  }
}

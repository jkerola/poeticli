// Package imports:

// Dart imports:
// import 'dart:cli';

// Dart imports:
import 'dart:io';

// Package imports:
import 'package:args/command_runner.dart';
import 'package:cli_util/cli_logging.dart';
import 'package:dart_console/dart_console.dart';
import 'package:hive/hive.dart';

// Project imports:
import '../models/arg_strings.dart';
import '../models/poem_obj.dart';
import '../utils/template_utils.dart';

// consts
final argStrings = ArgStrings();

// TODO test multieditor configuration
// TODO cleanup
class PoemCommand extends Command {
  @override
  final name = argStrings.argPoem;
  @override
  final description = argStrings.helpPoem;
  final console = Console();
  final String filename = '.tmp.ptcli';
  PoemCommand();
  @override
  void run() async {
    final config = await Hive.openBox('config');
    // Create a temporary file for the editor
    File temp = File('./$filename');
    // Display helpful information
    temp.writeAsStringSync(argStrings.infoEditor);

    // Start editor process, pause poeticli until editor has closed
    var editorProcess = await Process.start(config.get('editor'), [filename],
        mode: ProcessStartMode.inheritStdio);
    final exitCode = await editorProcess.exitCode;
    if (exitCode != 0) {
      logger.stderr(argStrings.errEditor);
    }
    // Parse temporary file lines
    var lines = await temp.readAsString();
    var splitLines = lines.trim().split("\n").toList();
    // Remove comment lines
    var parsed = splitLines.where((e) => !e.startsWith("#")).toList();
    if (parsed.isEmpty) {
      logger.stdout("Empty file detected, poem discarded.");
    } else {
      // Create Poem object
      var poem = Poem()
        ..content = parsed.join('\n')
        ..date = DateTime.now();

      // Save poem
      Progress progress = logger.progress('Saving poem');
      var poemBox = await Hive.openBox('poems');
      await poemBox.add(poem);
      progress.finish(message: 'Done.');
    }
  }
}

// Package imports:
import 'package:args/command_runner.dart';
import 'package:hive/hive.dart';

// Project imports:
import '../models/arg_strings.dart';
import '../utils/template_utils.dart';

// consts
final argStrings = ArgStrings();

class ShowCommand extends Command {
  @override
  final name = argStrings.argShow;
  @override
  final description = argStrings.helpShow;

  ShowCommand() {
    argParser.addFlag(argStrings.showAllArg,
        help: argStrings.showAllHelp, abbr: 'a', negatable: false);
    argParser.addOption(argStrings.showPoemArg,
        abbr: 'p', help: argStrings.showPoemHelp);
  }
  @override
  void run() async {
    var poemBox = await Hive.openBox('poems');
    if (argResults!.wasParsed(argStrings.showAllArg)) {
      displayShowAllDetail();
    } else if (argResults!.wasParsed(argStrings.showPoemArg)) {
      try {
        var poemId = int.parse(argResults![argStrings.showPoemArg]);
        var poem = poemBox.getAt(poemId);
        displayPoem(poem);
      } catch (error) {
        logger.stderr(error.toString());
      }
    } else {
      displayShowAll();
    }
  }
}

// Package imports:
import 'package:args/command_runner.dart';

// Project imports:
import '../models/arg_strings.dart';
import '../utils/template_utils.dart';

// consts
final argStrings = ArgStrings();

class AboutCommand extends Command {
  @override
  final name = argStrings.argAbout;
  @override
  final description = argStrings.helpAbout;

  AboutCommand();

  @override
  void run() {
    displayAbout();
  }
}

// Package imports:
import 'package:test/test.dart';
import 'package:test_process/test_process.dart';

void main() {
  group("About command", () {
    const args = ['run', r'bin/poeticli.dart', 'about'];
    test("should display proper information", () async {
      final process = await TestProcess.start('dart', args);
      expectLater(process.stdout, emitsThrough('Write poetry on your CLI!'));
    });
  });
}

#!/bin/bash

set -e

# Prettier output
export TXT_BOLD="\e[1m"
export TXT_CLEAR="\e[0m"

# Analyze next version
export CURRENT_VERSION=$(convco version)
export NEXT_VERSION=$(convco version -b)

echo -e "${TXT_BOLD}=> Comparing builds: ${CURRENT_VERSION} -> ${NEXT_VERSION}${TXT_CLEAR}"
if [[ "$NEXT_VERSION" != "$CURRENT_VERSION" ]]
then
    echo -e "${TXT_BOLD}=> Setting variables for PoetiCLI ${NEXT_VERSION}${TXT_CLEAR}"
    export GIT_VERSION="v${NEXT_VERSION}"
    export BUILD_FILENAME="poeticli-${NEXT_VERSION}-linux-x64.zip"

    echo -e "${TXT_BOLD}=> Creating temporary git tag${TXT_CLEAR}"
    git tag $GIT_VERSION

    echo -e "${TXT_BOLD}=> Generating release notes${TXT_CLEAR}"
    convco changelog -c .versionrc.yaml | diff --changed-group-format='%<' --unchanged-group-format='' - CHANGELOG.md | tee release_notes.md

    echo -e "${TXT_BOLD}=> Generating changelog${TXT_CLEAR}"
    convco -c .versionrc.yaml changelog > CHANGELOG.md

    echo -e "${TXT_BOLD}=> Removing temporary tag${TXT_CLEAR}"
    git tag -d $GIT_VERSION

    echo -e "${TXT_BOLD}=> Updating pubspec.yaml${TXT_CLEAR}"
    cider version $NEXT_VERSION

    echo -e "${TXT_BOLD}=> Downloading packages${TXT_CLEAR}"
    dart pub get

    echo -e "${TXT_BOLD}=> Building application${TXT_CLEAR}"
    dart compile exe bin/poeticli.dart -o poeticli

    echo -e "${TXT_BOLD}=> Exporting variables${TXT_CLEAR}"
    echo "BUILD_VERSION=${NEXT_VERSION}" >> build.env
    echo "BUILD_FILENAME=${BUILD_FILENAME}" >> build.env
    
    echo -e "${TXT_BOLD}=> Compressing build for upload"
    zip build.zip poeticli LICENSE README.md release_notes.md

    echo -e "${TXT_BOLD}=> Committing CHANGELOG.md to repository${TXT_CLEAR}"
    git clone "https://gitlab-ci-token:${POETICLI_ACCESS_TOKEN}@${CI_REPOSITORY_URL#*@}" poeticli_ci
    cp CHANGELOG.md pubspec.yaml poeticli_ci/
    cd poeticli_ci
    git add CHANGELOG.md pubspec.yaml
    git commit -m "docs: update changelog, pubspec from CI/CD"

    echo -e "${TXT_BOLD}=> Recreating git tag w/ changelog${TXT_CLEAR}"
    git tag $GIT_VERSION

    echo -e "${TXT_BOLD}=> Pushing changes to remote repository${TXT_CLEAR}"
    git push -o ci.skip "https://gitlab-ci-token:${POETICLI_ACCESS_TOKEN}@${CI_REPOSITORY_URL#*@}" $CI_DEFAULT_BRANCH > /dev/null 2>&1
    git push -o ci.skip "https://gitlab-ci-token:${POETICLI_ACCESS_TOKEN}@${CI_REPOSITORY_URL#*@}" $GIT_VERSION > /dev/null 2>&1

else
    echo -e "${TXT_BOLD}=> No version bump detected! Skipping build...${TXT_CLEAR}"
fi
